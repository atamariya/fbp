var block, ctxt = [];
exports.execute = e=>{
    console.log(e);
    if (typeof(e.payload) == "string" && !e.payload.startsWith("connect")) {
        block = ctxt.pop();
        if (e.payload.startsWith("//")) {
            // Skip comments
        } else if (block == "code" && e.payload.indexOf("<<") != 0) {
            e.code = e.payload + "\n";
        } else if (block == "flow" && e.payload.trim() == "") {
            e.marker = "end";
            block = ctxt.pop();
        } else {
            var parts = e.payload.split(",").map(Function.prototype.call, String.prototype.trim);
            e.nodes = [];
            if (parts.length == 1) {
                var i = parts[0].indexOf("<<");
                if (i > 0) {
                    e.subflow = parts[0].substring(0, i);
                    ctxt.push(block);
                    block = "code";
                    e.marker = "start";
                } else if (i == 0) {
                    e.marker = "end";
                    block = ctxt.pop();
                } else if (block != "code") {
                    e.subflow = parts[0];
                    ctxt.push(block);
                    block = "flow";
                    e.marker = "start";
                } else {
                    e.code = parts[0];
                }
            } else
                e.nodes.push({
                    name: parts[0],
                    type: parts[1],
                    args: parts.splice(2)
                });
        }
        e.payload = null;
        console.log(e);
        if (block)
            ctxt.push(block);
    }
    return e;
}
