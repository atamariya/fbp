function init(name) {
    this.name = name;
}
function start(cb, cl, e) {
    var name = this.name;
    var lineReader = require('readline').createInterface({
        input: require('fs').createReadStream(this.name)
    });

    cb({marker: "start"});
    lineReader.on('line', (m) => {
        var t = {...e};
        t.payload = m;
        cb(t);
//         console.log(t);
    });
    return new Promise((resolve, reject) => {
        lineReader.on('close', () => {
            var t = {...e};
            t = cl(t);
            resolve(t);
        });
    });
}

module.exports = function() {
    this.init = init;
    this.start = start;
}
