exports.init = function(e) {
    this.uri = e;
}
exports.execute = e=>{
    // console.debug(e);
    var msg = e;
    // /prefix/entity/id
    var parts = msg.payload.uri.split("/");
    msg.payload.entity = parts[2];
    switch (msg.payload.method) {
    case "GET":
        if (parts.length == 4) {
            msg.payload.id = parts[3];
            msg.payload.action = "find";
        } else
            msg.payload.action = "list";
        break;
    case "POST":
        if (parts.length == 4) {
            msg.payload.id = parts[3];
        } else {
            msg.payload.id = msg.payload.data.id;
        }
        msg.payload.action = "create";
        break;
    case "PUT":
        msg.payload.id = parts[3];
        msg.payload.action = "update";
        break;
    case "DELETE":
        msg.payload.id = parts[3];
        msg.payload.action = "delete";
        break;
    }
    msg.payload.uri = this.uri;
    return e;
}
