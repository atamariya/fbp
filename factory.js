var packages = ["parseNodes", "append", "genCode", "vmscript", ];
var vars = [];
var initargs = [];

var core = require("./core")();
var res = core.init(packages, initargs, vars)

exports.execute = function(e) {
    var res1 = res.then(()=>core.execute(e));
    res1.then((e)=>console.log(e));
    // Additional code to process res
    return res1;
}
