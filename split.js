var async = require("async");
var fns;

exports.init = function(...args) {
	//console.log(args);
	fns = args;
}

exports.execute = e => {
	//console.log(fns)
	async.parallel(fns.forEach(f => require(`./${f}`).execute(e)));
	return e;
}
