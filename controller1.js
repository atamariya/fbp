var packages = ["lookup", "factory"];
var vars = [];
var initargs = [];

var core = require("./core")();
var res = core.init(packages, initargs, vars);
exports.execute = function(e) {
    var res1 = res.then(()=>core.execute(e));
    res1 = res1.then(m=>{
        console.log(m);
        // Additional code to process res
        if (m && typeof(m.payload) == "object" && m.payload.uri) {
            m = this.execute(m);
        }
        return m;
    }
    )
    return res1;
}
