/1,test2,/2
/2,renderer,a.tmpl
/3,c

/currency,renderer,html/currency.html,layout/main.html
/input,renderer,html/input.html
/list,renderer,html/list.html
/product,renderer,html/product.html
/supply,renderer,html/supply.html
/txn,renderer,html/txn.html
/uom,renderer,html/uom.html
/user,renderer,html/user.html

-- Wildcard URIs
/rest/*,restHandler,app

-- URIs not starting with / is inaccessible via direct request.
-- Use these for access control e.g. admin/uri
app,appHandler