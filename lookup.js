exports.init = function(e) {
    var packages = ["lineReader", "stripComment", "store", "stdout", ];
    var vars = [];
    vars[3] = "m";
    var initargs = [];
    initargs[0] = ["mapping.txt"];
    initargs[1] = ["--"];

    var core = require("./core")();
    var res = core.init(packages, initargs, vars).then(()=>core.execute());
    // Additional code to process res
    res.then(e=>{
        console.log(e);
        return this.m = e.payload;
    }
    );
    return res;
}
exports.execute = function(e) {
    console.log(e);
    var r = this.m[e.payload.uri];
    if (r) {
        e.payload = r;
    } else {
        // Look for regex match
        for (var k in this.m) {
            var find = e.payload.uri.match("^" + k);
            if (find) {
                e.payload = this.m[k];
                break;
            }
        }
    }
    return e;
}
