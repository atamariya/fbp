exports.init = function(e) {
    this.str = e;
}
exports.execute = function(e) {
    if (e.payload && !e.payload.startsWith(this.str)) {
        return e;
    }
}
