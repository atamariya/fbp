var vm = require('vm');
var i = 0;
exports.execute = e=>{
    console.log(e);
    if (e.payload) {
        var code = `(e=> {${e.payload.code}})(e)`;
        var out = vm.runInNewContext(code, {
            require: require,
            console: console,
            e : e.payload.in
        }, e.payload.subflow || "script" + i++);
        if (typeof out == 'object' && typeof out.then == 'function') {
            out = out.then(m=>{
                console.log(m);
//                 e.payload = m;
                return m;
            }
            );
        }
        return out;
    }
}
;
