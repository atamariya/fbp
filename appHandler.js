var db = require("./cloudant");
var _ = require("lodash");

exports.execute = e=>{
    // console.debug(e);
    eval(e.payload);

    var msg = // {payload : {type : "customer", code: "c1"}};
    // {payload : {type : "customer", code: "c1", created_at: new Date()}};
    // {payload : {type : "demand", code: "p1", quantity: 4, customer: {code: "c1"}}};
    // {payload : {type : "demand", code: "p1", quantity: 4, customer: {code: "c1"}
    // 		, expiry: new Date()//"2020-06-04T07:19:16.757Z")
    // 	       }};
    // {payload : {type : "order", customer: {code: "c1"}}};
    // {payload : {type : "order", customer: {code: "c2"}}};
    // {payload : {type : "supply", code: "p1"}};
    // {payload : {type : "supply", code: ["p1", "p2"]}};
    	{payload : {type : "addtocart", code: "p1", quantity: 4}};

    console.log(msg)
    var t = 
    	{payload : {type : "supply", code: ["p1", "p2"]}};
    console.log(_.merge(msg, t))

    // action should be assigned by API endpoint
//     msg.action = "update";
    msg = e;
    msg.action = msg.payload.action;

    switch(msg.action) {
        case "find":
        case "list":
            var select = {};
            if (msg.payload.entity)
                select.entity = msg.payload.entity;
            if (msg.payload.id)
                select.id = msg.payload.id;
            var params = msg.payload.data;
            msg.payload = {selector: select};
            if (params.limit)
                msg.payload.limit = parseInt(params.limit);
//             if (params.page) {
//                 var pageSize = params.pageSize || 2;
//                 pageSize = parseInt(pageSize);
//                 msg.payload.skip = parseInt(params.page) * pageSize;
//                 msg.payload.limit = pageSize;
//             }
            break;
        case "delete":
            break;
        default:
            var doc = msg.payload.data;
            doc.entity = msg.payload.entity;
            if (msg.payload.id)
                doc.id = msg.payload.id;
            msg.payload = doc;
    }

    switch (msg.payload.type1) {
    case "customer":
        msg._id = msg.payload.code;
        if (msg.payload.created_at)
            msg.payload.lastupdated = new Date();
        else
            msg.payload.created_at = new Date();
        break;
    case "cart":
        // Order item update
        msg._id = msg.payload.customer.code + "-" + msg.payload.code;
        if (!msg.payload.expiry) {
            // Give the customer 5 min to finish txn - only once
            var now = new Date();
            now.setMinutes(now.getMinutes() + 5);
            msg.payload.expiry = now;
        }
        break;
    case "checkout":
        // Reserve inventory before payment - expires after 1 min
        // Allocate inventory after payment
        msg._id = msg.payload.customer.code + "-" + msg.payload.code;
        // Give the customer 1 min to finish txn
        var now = new Date();
        now.setMinutes(now.getMinutes() + 1);
        msg.payload.expiry = now;
        break;
    case "addtocart":
        // Order item update
        msg._id = msg.payload.customer.code + "-" + msg.payload.code;
        break;
    case "supply":
        // inventory view
        msg.action = "view";
        msg.payload.design = "ddoc";
        msg.payload.view = "inventory";
        var code;
        if (typeof (msg.payload.code) == "string")
            code = [msg.payload.code];
        else
            code = msg.payload.code;
        msg.payload.params = {
            group: true,
            keys: code
        };
        break;
    case "order":
        // cart view
        msg.action = "view";
        msg.payload.design = "ddoc";
        msg.payload.view = "customer";
        var code = msg.payload.customer.code;
        msg.payload.params = {
            group: true,
            startkey: [code, 1],
            endkey: [code, {}]
        };
        break;
     default:
//         throw "Unsupported";
    }

    msg = db.execute(msg);
    msg.then(m=>console.log(m));
    return msg;
}
