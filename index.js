var file = process.argv[2] || "init";
var packages = ["lineReader", "parseNodes", "parseConnections", "append", "genCode", "writer", "vmscript", ];
var vars = [];
var initargs = [];
initargs[0] = [file + ".fbp"];
initargs[4] = [file, true];

var core = require("./core")();
var res;
var e = e || {};
res = core.init(packages, initargs, vars);
res = res.then(() => core.execute(e));
res = res.then((e) => {
    console.log(e);
    // Additional code to process res
    // Returned value is the value of res
    return e;
    });
return res;
