// Concatenates string payloads and merges JSON payloads
function execute(e) {
    var obj = this.obj;
    var ctxt = this.ctxt;
    if (typeof e.payload == "string") {
        obj = (obj || "") + e.payload;
    } else if (typeof e.payload == "number") {
        obj = (obj || 0) + e.payload;
    } else {
        obj = obj || {};
        for (var attrname in e) {
            // Don't merge special attributes
            if (attrname == "marker"
		|| attrname == "payload"
		|| attrname == "in"
		|| attrname == "env")
                continue;
            if (obj[attrname]) {
                if (e[attrname]instanceof Array)
                    obj[attrname] = obj[attrname].concat(e[attrname]);
                else
                    obj[attrname] = obj[attrname] + e[attrname];
            } else
                obj[attrname] = e[attrname];
        }
    }
    if (e.marker == "start") {
        ctxt.push(obj);
        obj = null;
    }
    this.obj = obj;
}

function end(e) {
    var obj = this.obj;
    var ctxt = this.ctxt;
    // Reset end marker to continue processing
    // Pass e.payload from end marker as e.in to be used as arg for calling flow
    var tmp = ctxt.pop();
    if (tmp) {
        // Merge everything since start
        this.execute(tmp);
        if (ctxt.length == 0 && tmp.payload) {
            // tmp.payload indicates we have recursive marker
// 	    tmp.in = e.in;
            return tmp;
        }
    }
    if (ctxt.length > 1) {
    	// Top needs to be replaced
    	ctxt.pop();
    }
    if (ctxt.length == 1) {
        var tmp = ctxt.pop();
        if (!tmp.payload)
            tmp.payload = [];
        tmp.payload.push(obj);
        ctxt.push(tmp);
        this.obj = null;
    } else {
//     	e.in = e.payload;
        e.payload = obj;
        e.marker = null;
        this.obj = null;
        return e;
    }
}

module.exports = function() {
	this.end = end;
	this.execute = execute;
	this.obj = null;
	this.ctxt = [];
}