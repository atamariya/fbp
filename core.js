function init(x, y, z) {
    var packages = x;
    var initargs = y;
    this.vars = z;

    var r = packages.map((e,i)=>{
        var fn = require(`./${e}`);
        var res;
        if (typeof(fn) == "function")
            fn = new fn();
        if (fn.hasOwnProperty("init"))
            res = fn.init.apply(fn, initargs[i])
        this.fns.push(fn);
        return res;
    }
    );
    return Promise.all(r);
}
function execute(e) {
    if (typeof (e) != "object")
        e = {payload: e};
    var t = {...e};
    this.in = e.in = t.in || t;
    if (this.fns[0].hasOwnProperty("start")) {
        // For components emitting events
        e = this.fns[0].start(this.cb.bind(this), this.cl.bind(this), e);
    } else {
        e = this.fns[0].execute(e);
        if (e) {
//             if (!e.in) {
//                 // in has original request which triggered the flow
//                 // Allow override for mock requests
//                 e.in = t.in || t;
//             }
            e = this.cb(e);
            if (!e) {
                // if e is undefined, we need to send end marker
                e = this.cl(t);//{in: t.in || t});
            }
        }
    }
    return Promise.resolve(e);
}
function seq(e, fns, i, res) {
    var i = i || 0;
    // Store e.payload for each step
    // Typically tracks end marker payload
    var res = res || {};
    this.in = e.in || this.in;
    while (++i < fns.length && e) {
        var fn = null;
        if (e.marker == "end") {
            if (fns[i].hasOwnProperty("end"))
                fn = fns[i].end;
        } else if (fns[i].hasOwnProperty("execute"))
            fn = fns[i].execute;

        // res stores output from previous steps which can be
        // retrieved from e.env
        var j = i - 1;
        if (this.vars[j])
            res[this.vars[j]] = e.payload;
        res[j] = e.payload;

        if (fn) {
            if (typeof e.then == 'function') {
                e = e.then(m=>{
                    // Need to pass res from previous steps
                    return this.seq(m, fns, j, res);
                }
                );
                break;
            } else if (e instanceof Array) {
                if (e.length == 0) {
                    e = null;
                } else {
                    // Need to pass res from previous steps
                    e = e.map(m=>this.seq(m, fns, j, res));
                    // Need not wait for all Promise resolutions
                    e = Promise.race(e);
                }
                break;
            } else {
                e.env = res;
                e.in = this.in;
                e = fn.call(fns[i], e);
                if (e) {
                    // Store the last result in the loop
                    if (typeof e.then == 'function') {
                        e.then(e=>{
                            res[i] = e && e.payload;
                        });
                    } else {
                        // Handle malformed output
                        res[i] = e.payload;
                    }
                }
            }
        }
    }
    return e;
}
function cb(e) {
//     if (typeof (e) != "object")
//         e = {
//             payload: e
//         };
    e = this.seq(e, this.fns);
    return e;
}
function cl(e) {
//     if (typeof (e) != "object")
//         e = {payload: e};
    e.marker = "end";
    e = this.seq(e, this.fns);
    if (e) {
        // Delete the marker to end the subflow
        delete e.marker;
    }
    return e;
}

module.exports = ()=>{
    return {
        init: init,
        execute: execute,
        cb: cb,
        cl: cl,
        seq: seq,
        fns: []
    }
}
