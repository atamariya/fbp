var cache = []
  , size = 100;
exports.init = e=>{
    size = e;
}
exports.execute = e=>{
    if (e.payload && e.compare) {
        var cmp = e.compare;
        var key = e.key || e.payload;
        var v = cache.find(m=>cmp(key, m.key));
        if (!v) {
            v = {
                key: key,
                val: e.payload,
                reads: 1
            };
            cache.push(v);
            if (cache.length > size) {
                // MFU
                cache.sort((a,b)=>{
                    return -b.reads + a.reads;
                }
                );
                cache.shift();
            }
        } else {
            v.reads++;
        }
        return v.val;
    }
}
