exports.init = function(e) {
    this.uri = e;
}
exports.execute = function(m) {
    // payload for next step
    m.payload = {uri: this.uri};
    // Modify original request content
    m.in.name = "g";
    return m;
}
