var http = require('http');
var url = require('url');

// var server = http.createServer(function(req, res) {
//     //parse the url and query string from the request 
//     var url_parts = url.parse(req.url, true);
//     console.log(url_parts);

//     //if the callback query parameter is set, we return the string (or object)
//     if (url_parts.query.callback) {
//         var str = "Hi man";
//         res.writeHead(200, {
//             'Content-Type': 'text/html'
//         });
//         res.end(url_parsed.query.callback + '("' + str + '")');

//         //if it's not set, let's return a 404 error
//     } else {
//         res.writeHead(404, {
//             'Content-Type': 'text/html'
//         });
//         res.end('404 Error');
//     }
// }).listen(process.env.PORT || 8080);

// server.on('clientError', (err, socket) => {
//   if (err.code === 'ECONNRESET' || !socket.writable) {
//     return;
//   }

//   socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
// });

// response.writeHead(200, { 'Content-Type': 'text/plain',
//                           'Trailer': 'Content-MD5' });
// response.write(fileData);
// response.addTrailers({ 'Content-MD5': '7895bf4b8828b55ceaf47747b4bca667' });
// response.end();
// return

var express = require('express');
var app = express();

// var cookieParser = require('cookie-parser');
// // load the cookie-parsing middleware
// app.use(cookieParser());

// parse application/x-www-form-urlencoded
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());

var session = require('express-session');
app.use(session({
    secret: "sessionSecret",
    saveUninitialized: true,
    resave: true
}));

// var passport = require('passport');
// app.use(passport.initialize());
// app.use(passport.session());

var packages = ["controller1"];
var vars = [];
var initargs = [];

var core = require("./core")();
var res;
res = core.init(packages, initargs, vars);
var controller = function (req, out, next) {
//     console.log(req);
    var contentType = "json";
    var uri = req._parsedUrl.pathname;
    if (uri.endsWith(".html")) {
        contentType = "html";
        uri = uri.match("(.*).html")[1];
    }

    var payload = {payload: {}};
    var data = req.body;
    if (req.method == "GET") {
        // Use query object for GET method
        data = req.query;
    }
    payload.payload.method = req.method;
    payload.payload.contentType = contentType;
    payload.payload.uri = uri;
    payload.payload.data = data;

    var res1;
    res1 = res.then(() => core.execute(payload));
    res1.then((e) => {
        if (e)
            out.send(e.payload);
        else
            next();    
    })
    .catch(e=> {
        if (contentType == "json")
            out.status(500).send({ error: e});
        else
            next(e);
        });
}
app.use(controller);
  
app.listen(8080 , function(){
    console.log("working on the Port 8080"); 
});

// app.post('/login', passport.authenticate('local', { successRedirect: '/',
//                                                     failureRedirect: '/login' }));

// app.get('/setColor', function(req, res, next) {
//     req.session.favColor = 'Red';
//     res.send('Setting favourite color ...!');
// });

// app.get('/getColor', function(req, res, next) {
//     res.send('Favourite Color : ' + (req.session.favColor == undefined ? "NOT FOUND" : req.session.favColor));
// });

