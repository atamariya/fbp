require("dotenv").config();
var Cloudant = require("nano");
// URL denotes an existing DB
var db = Cloudant({
    url: process.env.url
});

db.update = function(obj, check) {
    var db = this;
    var msg = {
        payload: obj
    };
    var key = {selector: {id: obj.id, entity: obj.entity}};
    return db.find(key).then((existing) => {
        if (check) {
            if (existing.docs.length > 0) {
                throw "Duplicate id";
            }
        } else {
            existing = existing.docs[0];
            obj._id = existing._id;
            obj._rev = existing._rev;
        }

        return db.insert(obj, obj._id).then((data)=>{
            msg.payload._id = data.id;
            msg.payload._rev = data.rev;
            console.log(data);
            return msg;
        }
        );
    });
}

db.delete = function(obj) {
    var db = this;
    var msg = {
        payload: {}
    };
    var key = {selector: {id: obj.id, entity: obj.entity}};
    return db.find(key).then((existing) => {
        existing = existing.docs[0];
        key = existing._id;
        rev = existing._rev;

        return db.destroy(key, rev).then((data)=>{
            msg.payload._id = data.id;
            msg.payload._rev = data.rev;
            console.log(data);
            return msg;
        }
        );
    });
}

function execute(msg) {
    console.log(msg);

    var res;
    switch (msg.action) {
    case "view":
        res = db.view(msg.payload.design, msg.payload.view, msg.payload.params)
        .then((data)=>{
            msg.payload = data;
            console.log(data);
            return msg;
        }
        );
        break;
    case "find":
        msg.payload.limit = 1;
    case "list":
        // Read based on selector e.g. entity id (stored as id)
        //{ selector: { name:'Alice' }}
        res = db.find(msg.payload).then((data)=>{
            if (msg.payload.limit == 1) {
                msg.payload = data.docs[0];
            } else {
                msg.payload = data.docs;
            }
            console.log(data);
            return msg;
        }
        );
        break;
    case "create":
    case "update":
        if (msg.payload._id)
            res = db.insert(msg.payload);
        else
            res = db.update(msg.payload, true);
        break;
    case "read":
        // Read based on DB id (stored as _id)
        res = db.get(msg.payload.id).then((data)=>{
            msg.payload._id = data._id;
            msg.payload._rev = data._rev;
            msg.payload = data;
            console.log(data);
            // node.send(msg);
            return msg;
        }
        );
        break;
    case "delete":
        res = db.delete(msg.payload);
        break;
    }
    return res;
}

exports.execute = execute;
