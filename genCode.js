var filename, single;
function loadTempModule(id, src) {
    // Load src as a temp module which can be used as require(./id)
    var Module = require('module');
    var m = new Module(id);
    //src = 'module.exports = {a: 42}'
    var method, src_load;
    // Arrow function doesn't have "this" bound. Use normal function.
    src_load = `exports.execute = function(e) {\n${src}}\n`;
    if (single) {
        // Single module with each subflow as a function - typically init and execute
        var i = id.indexOf("(");
        if (i < 0)
            i = id.length;
        var arg = id.substring(i).replace(":", ",") || "(e)";
        method = id.substring(0, i);
        src = `exports.${method} = function${arg} {\n${src}}\n`;
    } else {
        // Each subflow is a module with execute()
        src = src_load;
    }

    var loadfn = require('vm').runInThisContext(Module.wrap(src_load));
    // loadfn ends with a semi-colon. Hence invoke separately
    loadfn(m.exports, require, m, __filename, __dirname);

    var request = `./${id}`,paths = [""];
    var cacheKey = [request, __dirname].join("\x00");
    Module._pathCache[cacheKey] = request;
    require.cache[request] = m;

    //console.log(Module._pathCache);
    //console.log(m)
    //var s = require(request);
    //console.log(s.a)
    return src;
}

exports.init = (x,y)=>{
    filename = x;
    single = y;
}

exports.execute = e=>{
    if (e.payload) {
        //         console.log(e);
        // Generate output for subflows
        var code = '', main = '', s;
        var t = [];
        if (!(e.payload instanceof Array)) {
            e.payload = [e.payload];
        }
        e.payload.map(m=>{
            s = generate(m, e.in);
            if (!s)
                return;
            if (m.subflow) {
                if (filename && !single) {
                    var res = {
                        payload: {}
                    };
                    res.payload.content = s;
                    res.payload.file = m.subflow + ".js";
                    t.push(res);
                }
            } else {
                main += s;
            }
            code += s;
        }
        );

        // Generate output for main
        var res = {
            payload: {}
        };
        if (main) {
            res.payload.code = main;
            res.payload.in = e.in;
            t.push(res);
        }
        if (filename) {
            res.payload.content = single ? code : main;
            res.payload.file = filename + ".js";
            res.append = single;
        }


        //         console.log(e);
        return t;
    }
}

function generate(e, arg) {
    var code = e.code;
    if (!code) {
        if (e.nodes && e.nodes.length > 0) {
            var seq = "", vars = "", init = "";
            e.nodes.forEach((e,i)=>{
                console.log(e);
                if (e.type) {
                    e.type = e.type.replace(" ", "_");
                    var j = e.type.indexOf("=");
                    var v;
                    if (j > 0) {
                        v = e.type.substring(0, j);
                        e.type = e.type.substring(j + 1);
                        vars += `vars[${i}] = "${v}";\n`;
                    }
                    seq += `"${e.type}", `;
                    if (e.args && e.args.length > 0) {
                        init += `initargs[${i}] = [`
                         + e.args.map(s=>!isNaN(s) ? s:`"${s}"`).join(", ")
                         + '];\n';
                    }
                }
            }
            );
            seq = "var packages = [" + seq + "];\n";
            var subflow_ret = "return res;";//e.subflow ? "return res;" : "";
            var main = `
var core = require("./core")();
var res;
res = core.init(packages, initargs, vars);
res = res.then(() => core.execute(${arg ? 'e': ''}));
res = res.then((e) => {
    console.log(e);
    // Additional code to process res
    // Returned value is the value of res
    return e;
    });
${subflow_ret}
`;
            code = seq + "var vars = [];\n" + vars + "var initargs = [];\n" + init + main;
        }
    }

    if (e.subflow) {
        code = loadTempModule(e.subflow, code);
    }
    console.log(code);
    return code;
}
