var fs = require('fs');

exports.execute = e=>{
    if (!e.payload || !e.payload.file)
        return e;

    var write = fs.writeFile;
    var file = e.payload.file;
    if (e.payload.append)
        write = fs.appendFile;

    write(file, e.payload.content, function(err) {
        if (err)
            throw err;
        console.log('Saved ' + file);
    });
    return e;
}
