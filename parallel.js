/**
Use parallel when subtasks are computation tasks and the results are
needed to execute next step. If results are not needed immediately,
use split
*/
var async = require("async");
var fns, aliases;

exports.init = function(...args) {
    //console.log(args);
    fns = args.filter((e,i)=>i % 2 != 0);
    aliases = args.filter((e,i)=>i % 2 == 0);
}

exports.execute = e=>{
    //console.log(fns)
    fns = fns.map(f=>{
        return cb=>{
            cb(null, require(`./${f}`).execute(e));
        }
    }
    );
    return async.parallel(fns).then(res=>{
        e = e || {};
        res.forEach((m,i)=>{
            e[aliases[i]] = m.payload;
        }
        );
        console.log(e);
        return e;
    }
    );
}
