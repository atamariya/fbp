exports.execute = e => {
    if (typeof(e.payload) == "string" && e.payload.startsWith("connect")) {
	var parts = e.payload.split(",").map(Function.prototype.call, String.prototype.trim);
	e.connections = [];
	e.connections.push({ out: parts[1], in: parts[2]});
	e.payload = null;
	console.log(e);
    }
    return e;
}
