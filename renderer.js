var tag = /\$\{(\w+)\}/g;

exports.init = function(x, y) {
    this.file = x;
    this.layout = y;
}
exports.execute = function(e) {
    var res;
//     e.contentType = "html";
    if (typeof (e) == "object" && e.payload.contentType == "html") {
//         this.file = "html/currency.html"
//         this.layout = "layout/main.html"
        var body = readFile(this.file);
        var layout;
        if (this.layout)
            layout = readFile(this.layout);
        res = Promise.all([body, layout]).then(r=>{
            body = r[0].payload;
            layout = r[1].payload;
            var out;
            if (layout) {
                // If layout is defined, replace layout's body with view body
                if (typeof(e.payload) != "object")
                    e.payload = {};
                e.payload.data.body = body;
                body = layout.replace(tag, (m,k)=>e.payload.data[k] || '');
            }
            // Substitute var in body
            out = body.replace(tag, (m,k)=>e.payload.data[k] || '');

            console.log(out);
            e.payload = out;
            return e;
        }
        );
    } else {
        res = {payload: e.in};
    }
    return res;
}

function readFile(file) {
    var packages = ["lineReader", "appendNewline", "append"];
    var vars = [];
    var initargs = [];
    initargs[0] = [file];

    var core = require("./core")();
    var res;
    res = core.init(packages, initargs, vars);
    res = res.then(()=>core.execute());
    return res;
}
