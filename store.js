exports.execute = function(e) {
    if (!this.m)
        this.m = {};
    if (e.payload) {
        var a = e.payload.split(",");
        this.m[a[0]] = e.payload;
    }
}
exports.end = function(e) {
    e.payload = this.m;
    e.marker = null;
    return e;
}
